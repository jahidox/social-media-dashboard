import React, { useRef, useEffect, useState } from 'react';
import '../css/switch.css';

function Switch({ set_dark_mode, checked, set_checked }) {

  const ref = useRef(null);
  function handleChange(){
    set_checked(ref.current.checked);
    set_dark_mode(ref.current.checked);
  }

  return (
    <div className="dark-mode">
      <p className="dark-mode-title">Dark Mode</p>
      <input ref={ref} onChange={handleChange} type="checkbox" checked={checked} className="checkbox" id="checkbox" />
      <label className="switch" htmlFor="checkbox">

      </label>
    </div>
  )
}

export default Switch;
