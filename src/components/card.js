import React from 'react';
import '../css/card.css'

function Card({ username, followers, today_followers, icon, name }) {
  const card_class = `card ${ name }`
  return (
    <article className={ card_class }>
      <p className="card-title">
        <img src={ icon } alt="" />
            { username }
          </p>
      <p className="card-followers">
        <span className="card-followers-number">{ followers }</span>
        <span className="card-followers-title">Followers</span>
      </p>
      <p className="card-today">
        <img src="images/icon-up.svg" alt="" />
            { today_followers } Today
          </p>
    </article>
  )
}

export default Card;
